use trust_dns_resolver::config::*;
use trust_dns_resolver::Resolver;

use clap::Parser;
use execute::{shell, Execute};
use std::process::{Command, Stdio};

/// SuperSSH wrapper on SSH
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    destination: String,

    /// Forces ssh to use IPv4 addresses only.
    #[arg(long, short = '4')]
    force_ipv4: bool,

    /// Forces ssh to use IPv6 addresses only.
    #[arg(long, short = '6')]
    force_ipv6: bool,

    /// Enables forwarding of connections from an authentication agent such as ssh-agent(1).
    #[arg(long, short = 'A')]
    enable_authentication_agent: bool,

    /// Disables forwarding of the authentication agent connection.
    #[arg(long, short = 'a')]
    disable_authentication_agent: bool,

    /// Requests compression of all data (including stdin, stdout, stderr, and data for forwarded X11, TCP and UNIX-domain connections).
    #[arg(long, short = 'C')]
    compress: bool,

    /// Selects the cipher specification for encrypting the session. cipher_spec is a comma-separated list of ciphers listed in order of preference.
    #[arg(long, short = 'c')]
    cipher_spec: String,

    /// Specifies a local “dynamic” application-level port forwarding.
    #[arg(long, short = 'D')]
    bind_address: String,
    
    /// Append debug logs to log_file instead of standard error.
    #[arg(long, short = 'E')]
    log_file: String,

    /// Sets the escape character for sessions with a pty (default: ‘~’).
    #[arg(long, short = 'e')]
    escape_char: String,

    /// Specifies an alternative per-user configuration file.
    #[arg(long, short = 'F')]
    configfile: String,

    /// Requests ssh to go to background just before command execution.
    #[arg(long, short = 'f')]
    background: bool,

    /// Causes ssh to print its configuration after evaluating Host and Match blocks and exit.
    #[arg(long, short = 'G')]
    print_config: bool,

    /// Allows remote hosts to connect to local forwarded ports.
    #[arg(long, short = 'g')]
    allow_local_forwarded: bool,

    /// Specify the PKCS#11 shared library ssh should use to communicate with a PKCS#11 token providing keys for user authentication.
    #[arg(long, short = 'I')]
    pkcs11_lib: String,

    /// Selects a file from which the identity (private key) for public key authentication is read.
    #[arg(long, short = 'i')]
    identity_file: String,

    /// Connect to the target host by first making a ssh connection to the jump host described by destination and then establishing a TCP forwarding to the ultimate destination from there.
    #[arg(long, short = 'J')]
    jump_destination: String,

    /// Enables GSSAPI-based authentication and forwarding (delegation) of GSSAPI credentials to the server.
    #[arg(long, short = 'K')]
    enable_gssapi: bool,

    /// Disables forwarding (delegation) of GSSAPI credentials to the server.
    #[arg(long, short = 'k')]
    disable_gssapi: bool,

    /// Specifies that connections to the given TCP port or Unix socket on the local (client) host are to be forwarded to the given host and port, or Unix socket, on the remote side.
    #[arg(long, short = 'L')]
    local_bind_addr: String,

    /// Specifies the user to log in as on the remote machine.
    #[arg(long, short = 'l')]
    login_name: String,

    /// Places the ssh client into “master” mode for connection sharing.
    #[arg(long, short = 'M')]
    enable_master_mode: bool,

    /// A comma-separated list of MAC (message authentication code) algorithms, specified in order of preference.
    #[arg(long, short = 'm')]
    mac_spec: String,

    /// Do not execute a remote command. This is useful for just forwarding ports.
    #[arg(long, short = 'N')]
    no_command: bool,

    /// Redirects stdin from /dev/null (actually, prevents reading from stdin).
    #[arg(long, short = 'n')]
    in_from_null: bool,

    /// Control an active connection multiplexing master process.
    #[arg(long, short = 'O')]
    ctl_cmd: String,

    /// Can be used to give options in the format used in the configuration file.
    #[arg(long, short = 'o')]
    option: String,

    /// Port to connect to on the remote host.
    #[arg(long, short = 'p')]
    port: String,

    /// Queries for the algorithm specified.
    #[arg(long, short = 'Q')]
    query_option: String,

    /// Quiet mode. Causes most warning and diagnostic messages to be suppressed.
    #[arg(long, short = 'q')]
    quiet: bool,

    /// Specifies that connections to the given TCP port or Unix socket on the remote (server) host are to be forwarded to the local side.
    #[arg(long, short = 'R')]
    bind_addressa: String,

    /// Specifies the location of a control socket for connection sharing, or the strin “none” to disable connection sharing.
    #[arg(long, short = 'S')]
    ctl_path: String,

    /// May be used to request invocation of a subsystem on the remote system.
    #[arg(long, short = 's')]
    subsystem: bool,

    /// Disable pseudo-terminal allocation.
    #[arg(long, short = 'T')]
    disable_pseudoterminal: bool,

    /// Force pseudo-terminal allocation.
    #[arg(long, short = 't')]
    enable_pseudoterminal: bool,

    /// Verbose mode. Causes ssh to print debugging messages about its progress.
    #[arg(long, short = 'v')]
    verbose: bool,

    /// Requests that standard input and output on the client be forwarded to host on port over the secure channel.
    #[arg(long, short = 'W')]
    clientasdasfg: String,

    /// Requests tunnel device forwarding with the specified tun(4) devices between the client (local_tun) and the server (remote_tun).
    #[arg(long, short = 'w')]
    local_tun: String,

    /// Enables X11 forwarding.
    #[arg(long, short = 'X')]
    enable_x11_forwarding: bool,

    /// Disables X11 forwarding.
    #[arg(long, short = 'x')]
    disable_x11_forwarding: bool,

    /// Enables trusted X11 forwarding.
    #[arg(long, short = 'Y')]
    enable_x11_trusted_forwarding: bool,

    /// Send log information using the syslog(3) system module.
    #[arg(long, short = 'y')]
    syslog: bool,
}

fn main() {
    let args = Args::parse();

    let reso = Resolver::new(ResolverConfig::cloudflare_https(), ResolverOpts::default()).unwrap();
    let resp = reso.srv_lookup("_matrix-identity._tcp.cody.to").unwrap();
    let item = resp.iter().next().expect("nothing found");
    println!("{}", item.port());

    let mut command = shell("nano tempz");

    command.stdin(Stdio::inherit());
    command.stdout(Stdio::inherit());
    command.stderr(Stdio::inherit());

    let output = command.execute_output().unwrap();

    println!("{}", String::from_utf8(output.stdout).unwrap());
}
